<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('index')
		->with('products', Product::orderBy('price')->get());
});


//routes for maps
Route::post('add-store', array('uses' => 'StoreController@add'));
Route::get('find-store', array('uses' => 'StoreController@find'));
Route::get('get-location', function(){
	return View::make('location');
});

//routes for paypal
Route::post('checkout', array('uses' => 'PaypalController@checkout'));

Route::post('pay', array('uses' => 'PaypalController@pay'));


//add routes for add and find