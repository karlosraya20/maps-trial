<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <title>Maps</title>
    <style type="text/css">
        body
        {
            font-family: Arial;
            font-size: 10pt;
        }
    </style>
</head>
<body>
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false&libraries=places"></script>
    <script type="text/javascript">
        var msg, place, address, latitude, longitude;
        google.maps.event.addDomListener(window, 'load', function () {
            var places = new google.maps.places.Autocomplete(document.getElementById('txtPlaces'));
            google.maps.event.addListener(places, 'place_changed', function () {
                place = places.getPlace();
                address = place.formatted_address;
                latitude = place.geometry.location.A;
                longitude = place.geometry.location.F;
                msg = "Address: " + address;
                msg += "\nLatitude: " + latitude;
                msg += "\nLongitude: " + longitude;
            });
        });

        function setLocation() {
            document.getElementById("address").setAttribute('value', address);
            document.getElementById("latitude").setAttribute('value', latitude);
            document.getElementById("longitude").setAttribute('value', longitude);
            document.getElementById("print").innerHTML = msg;
        }

    </script>
    <div>
        <h1>Autocomplete</h1>
        <span>Location:</span>
        <input type="text" id="txtPlaces" style="width: 250px" placeholder="Enter a location" />
        
        <p>
        <button onclick="setLocation()">Set Location</button>
        {{Form::open(array('url'=>'add-store', 'method'=>'POST')) }}  
        {{Form::text('name', 'Enter Store Name') }}
        <input name="address" type="text" id="address" value="">
        <input name="latitude" type="text" id="latitude" value="">
        <input name="longitude" type="text"  id="longitude" value=""><br><br>
        {{Form::submit('Add Store') }}
        {{Form::close() }}
        </p>

        <a href="find-store"><button>Find Stores</button></a>
        <br><br>
        <a href="get-location">Get Current Location</a>

        <p id="print"></p>
    <div>

    <div style="background-color: #C0C0C0; margin: 100px 500px 100px; text-align: center; border-style: solid; border-color: black;">
            <h1>Cozee Paypal</h1>
            <form action="checkout" method="post" autocomplete="off">
                <label for="item">
                    Product <br>
                    <select name="product"> 
                        @foreach($products as $product)
                        <option value="{{{$product->name}}}">{{{$product->name . ' '. $product->price}}}</option>
                        @endforeach
                    </select>
                </label>
                <br><br>
                <input type="submit" value="Pay">
            </form>
        </div>   





</body>
</html>