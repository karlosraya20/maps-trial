<?php
	use PayPal\Api\Payer;
	use PayPal\Api\Item;
	use PayPal\Api\ItemList;
	use PayPal\Api\Details;
	use PayPal\Api\Amount;
	use PayPal\Api\Transaction;
	use PayPal\Api\RedirectUrls;
	use PayPal\Api\Payment;
	use PayPal\Api\PaymentExecution;

	require app_path().'/config/start.php';
	require base_path().'/vendor/autoload.php';

class PaypalController extends BaseController {
	public function checkout()
	{

		//define('SITE_URL', 'http://localhost:8000');

		$paypal = new \PayPal\Rest\ApiContext(
			new \PayPal\Auth\OAuthTokenCredential(
				'AfMARHHsMfPXrr7E1DbVCqU0VX7pz-UqxxCZgda2ROXJ_1v9Jd2RTG2jGB17L2QPkc5_OurAc4RJ8QEc',
				'EBWpiI6NbDVLyyPKDTDt4ARU9ef7wf-KTNYXzH8ttA7yC3CHizYWqOJEgcoQv7sjZKLXTG1Zp0A0_1sX'
			)
		);  

		$product = Input::get('product');
		$price = Product::where('name','=', $product)->firstOrFail()->price;

		$shipping = 20.00; 

		$total = $price + $shipping;

		$payer = new Payer();
		$payer->setPaymentMethod('paypal');

		$item = new Item();
		$item->setName($product)
			->setCurrency('PHP')
			->setQuantity(1)
			->setPrice($price);

		$itemList = new ItemList();
		$itemList->setItems([$item]);

		$details = new Details();
		$details->setShipping($shipping)
			->setSubtotal($price);

		$amount = new Amount();
		$amount->setCurrency('PHP')
			->setTotal($total)
			->setDetails($details);

		$transaction = new Transaction();
		$transaction->setAmount($amount)
			->setItemList($itemList)
			->setDescription('Cozee Trial Payment')
			->setInvoiceNumber(uniqid());

		$redirectUrls = new RedirectUrls();
		$redirectUrls->setReturnUrl(SITE_URL . '/pay?success=true')
			->setCancelUrl(SITE_URL . '/pay?success=false');

		$payment = new Payment();
		$payment->setIntent('sale')
			->setPayer($payer)
			->setRedirectUrls($redirectUrls)
			->setTransactions([$transaction]);

		try {
			$payment->create($paypal);
		} catch (Exception $e){
			die($e);
		}

		$approvalUrl = $payment->getApprovalLink();
		return '<a href="' . $approvalUrl . '">Click Here!</a>'; //comment out to test redirect
		return Redirect::away('$approvalUrl');
		}

	public function pay(){
		if(!isset($_GET['success'], $_GET['paymentId'], $_GET['PayerID'])) {
			die(); //ideally redirects to a page with an error notification
		}

		if((bool)$_GET['success']===false) { // checks if transaction failed
			die(); //ideally redirects to a page with an error notification
		}

		$paymentId = $_GET['paymentId'];
		$payerId = $_GET['PayerID'];

		$payment = Payment::get($paymentId, $paypal);

		$execute = new PaymentExecution();
		$execute->setPayerId($payerId);

		try {
			$result = $payment->execute($execute, $paypal);
		} catch (Exception $e){
			die($e); //ideally redirects to a page with an error notification
		}

		return 'Payment made. <a href="index.php">Back</a>'; //ideally be a redirect
		}
	}
