<?php

class StoreController extends BaseController {

	public function add()
	{

		$store = new Store();
		$store->name = Input::get('name');
		$store->address = Input::get('address');
		$store->latitude = Input::get('latitude');
		$store->longitude = Input::get('longitude');
		$store->save();
		return 'Store Added <a href="/">Back</a>';
	}


	public function find(){

		/*coordinates of quezon city*/
		$user_lat = 14.67;
		$user_lng = 121.04; 
		
		$store = DB::select('select name, ( 3959 * acos( cos( radians( '.$user_lat.' ) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians( '.$user_lng.' ) ) + sin( radians( '.$user_lat.' ) ) * sin( radians( latitude ) ) ) ) AS distance FROM store HAVING distance < 25.00 ORDER BY distance LIMIT 0 , 5');

		//gets the stores near the user in a 25km radius

		//$values = get_object_vars($store[0]);

		return $store;

	}
}
